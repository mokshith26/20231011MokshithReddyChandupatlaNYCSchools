package com.mokshith.nyc.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mokshith.nyc.R
import com.mokshith.nyc.common.MyDiffUtil
import com.mokshith.nyc.modelClasses.NycHighSchoolsModel
import com.mokshith.nyc.ui.DetailsActivity
import com.mokshith.nyc.ui.MainActivity
import kotlinx.android.synthetic.main.nyc_high_school_list.view.*

class MainActivityAdapterNycHighSchoolAdapter(
    val mainActivity: MainActivity
) : RecyclerView.Adapter<MainActivityAdapterNycHighSchoolAdapter.MyViewHolder>() {

    private var schoolsList: List<NycHighSchoolsModel>? = emptyList()

//    fun setSchoolList(schoolsList: List<NycHighSchoolsModel>){
//        this.schoolsList = schoolsList
//    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.adapterLayoutNYCHighSchool.setOnClickListener {
                val intent = Intent(mainActivity, DetailsActivity::class.java)
                intent.putExtra("dbn", schoolsList?.get(adapterPosition)?.dbn.toString())
                intent.putExtra("location", schoolsList?.get(adapterPosition)?.location.toString())
                intent.putExtra("phoneNumber", schoolsList?.get(adapterPosition)?.phoneNumber.toString())
                intent.putExtra("website", schoolsList?.get(adapterPosition)?.website.toString())
                intent.putExtra("schoolEmail", schoolsList?.get(adapterPosition)?.schoolEmail.toString())
                intent.putExtra("overviewParagraph", schoolsList?.get(adapterPosition)?.overviewParagraph.toString())
                intent.putExtra("schoolName", schoolsList?.get(adapterPosition)?.schoolName.toString())
                mainActivity.startActivity(intent)
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.nyc_high_school_list, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.schoolNameMainAdapter.text = schoolsList?.get(position)?.schoolName.toString()
    }

    override fun getItemCount(): Int {
        return if (schoolsList?.size == null){
            0
        }else{
            schoolsList?.size!!
        }
    }

    // I have used DiffUtils here to update the recyclerview data
    fun setSchoolData(newSchoolList: List<NycHighSchoolsModel>) {
        val diffUtils = schoolsList?.let { MyDiffUtil(it, newSchoolList) }
        schoolsList = newSchoolList
        diffUtils?.let { DiffUtil.calculateDiff(it) }?.dispatchUpdatesTo(this)
    }
}