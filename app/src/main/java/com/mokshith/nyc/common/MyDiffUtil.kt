package com.mokshith.nyc.common

import androidx.recyclerview.widget.DiffUtil
import com.mokshith.nyc.modelClasses.NycHighSchoolsModel

class MyDiffUtil(private val oldList : List<NycHighSchoolsModel>, private val newList : List<NycHighSchoolsModel>) : DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].dbn == newList[newItemPosition].dbn
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return when{
            oldList[oldItemPosition].dbn != newList[newItemPosition].dbn ->{
                false
            }
            oldList[oldItemPosition].schoolEmail != newList[newItemPosition].schoolEmail ->{
                false
            }
            oldList[oldItemPosition].primaryAddressLine1 != newList[newItemPosition].primaryAddressLine1 ->{
                false
            }
            else  -> true

        }
    }
}