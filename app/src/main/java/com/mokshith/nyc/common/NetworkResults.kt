package com.mokshith.nyc.common

sealed class NetworkResults<T>(val data: T? = null, val message : String? = null) {

    class Success<T>(data: T?): NetworkResults<T>(data)
    class Error<T>(message: String?, data: T? = null): NetworkResults<T>(data)
    class Loading<T>: NetworkResults<T>()
}
