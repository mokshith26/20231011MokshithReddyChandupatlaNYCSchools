package com.mokshith.nyc.di

import com.google.gson.GsonBuilder
import com.mokshith.nyc.common.ViewUtil
import com.mokshith.nyc.retrofitServices.WebService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetWorkModule {

    @Singleton
    @Provides
    fun provideRetrofit():Retrofit{
        return Retrofit.Builder()
            .baseUrl(ViewUtil.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
    }
    @Singleton
    @Provides
    fun providesAPI(retrofit: Retrofit): WebService{
        return retrofit.create(WebService::class.java)
    }
}