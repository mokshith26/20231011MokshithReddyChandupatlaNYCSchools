package com.mokshith.nyc.di

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mokshith.nyc.common.NetworkResults
import com.mokshith.nyc.modelClasses.NycHighSchoolsModel
import com.mokshith.nyc.retrofitServices.WebService
import javax.inject.Inject

class UserRepository @Inject constructor(private val webService: WebService) {

    private val _highSchoolLiveData = MutableLiveData<NetworkResults<List<NycHighSchoolsModel>>>()
    val highSchoolLiveData: LiveData<NetworkResults<List<NycHighSchoolsModel>>>
        get() = _highSchoolLiveData

    suspend fun getNycHighSchoolsData() {
        val response = webService.getNycHighSchoolsDataHilt()
        if (response.isSuccessful && response.body() != null) {
           _highSchoolLiveData.postValue(NetworkResults.Success(response.body()))
        } else if (response.errorBody() != null) {
            //Its known exception
            _highSchoolLiveData.postValue(NetworkResults.Error("Something went wrong"))
        } else {
            //Its unknown exception notify the developer
            _highSchoolLiveData.postValue(NetworkResults.Error("Something went wrong"))
        }
    }
}