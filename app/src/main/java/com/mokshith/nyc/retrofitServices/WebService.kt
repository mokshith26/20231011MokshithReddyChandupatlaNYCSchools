package com.mokshith.nyc.retrofitServices

import com.mokshith.nyc.modelClasses.GetSatScoreItem
import com.mokshith.nyc.modelClasses.NycHighSchoolsModel
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface WebService {

    @GET("resource/s3k6-pzi2.json")
    fun getNycHighSchoolsData(): Call<List<NycHighSchoolsModel>>

    /**Used coroutines and Hilt
      Suspend functions are typically used in conjunction with coroutines,
      which are lightweight threads of execution. Coroutines can be suspended and resumed,
      which allows them to be used to implement asynchronous code in a simple and efficient way.*/
    @GET("resource/s3k6-pzi2.json")
    suspend fun getNycHighSchoolsDataHilt(): Response<List<NycHighSchoolsModel>>

    @GET("resource/f9bf-2cp4.json")
    fun getSatScores(): Call<List<GetSatScoreItem>>?

}
