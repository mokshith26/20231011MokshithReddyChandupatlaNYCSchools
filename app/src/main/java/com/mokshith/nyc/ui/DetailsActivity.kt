package com.mokshith.nyc.ui

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.mokshith.nyc.common.ViewUtil
import com.mokshith.nyc.R
import com.mokshith.nyc.modelClasses.GetSatScoreItem
import com.mokshith.nyc.retrofitServices.RetrofitClient
import kotlinx.android.synthetic.main.activity_details.emailIdDetails
import kotlinx.android.synthetic.main.activity_details.locationDetails
import kotlinx.android.synthetic.main.activity_details.overViewDetails
import kotlinx.android.synthetic.main.activity_details.phoneNoDetails
import kotlinx.android.synthetic.main.activity_details.progressBarMath
import kotlinx.android.synthetic.main.activity_details.progressBarReading
import kotlinx.android.synthetic.main.activity_details.progressBarWriting
import kotlinx.android.synthetic.main.activity_details.satMathScoreDetails
import kotlinx.android.synthetic.main.activity_details.satReadingScoreDetails
import kotlinx.android.synthetic.main.activity_details.satWritingScoreDetails
import kotlinx.android.synthetic.main.activity_details.schoolNameDetails
import kotlinx.android.synthetic.main.activity_details.scoresLayout
import kotlinx.android.synthetic.main.activity_details.websiteDetails
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailsActivity : AppCompatActivity() {

    private lateinit var location: String
    private lateinit var phoneNumber: String
    private lateinit var website: String
    private lateinit var schoolEmail: String
    private lateinit var overviewParagraph: String
    private lateinit var schoolName: String
    private lateinit var dbn: String
    private val getSatScoreItem: MutableList<GetSatScoreItem> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        //the below code is to display back icon on the App Bar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        val extras = intent.extras
        if (extras != null) {
            dbn = extras.getString("dbn").toString()
            location = extras.getString("location").toString()
            phoneNumber = extras.getString("phoneNumber").toString()
            website = extras.getString("website").toString()
            schoolEmail = extras.getString("schoolEmail").toString()
            overviewParagraph = extras.getString("overviewParagraph").toString()
            schoolName = extras.getString("schoolName").toString()
        }
        locationDetails.text = location
        phoneNoDetails.text = phoneNumber
        websiteDetails.text = website
        emailIdDetails.text = schoolEmail
        overViewDetails.text = overviewParagraph
        schoolNameDetails.text = schoolName
        if (ViewUtil.isNetworkAvailable(this)){
            scoresLayout.visibility = View.VISIBLE
            callScoresAPI()
        }else{
            scoresLayout.visibility = View.GONE
            ViewUtil.showToast(this, "There is no Internet please try again")
        }
    }

    private fun callScoresAPI() {
        textViewVisibility(View.GONE)
        progressBarVisibility(View.VISIBLE)
        // I used retrofit for networking because it is Easy to use and maintain & Supports a wide range of HTTP features, including POST requests, multipart uploads, and custom response types
        val service = RetrofitClient().webService
        service.getSatScores()?.enqueue(object : Callback<List<GetSatScoreItem>> {
            override fun onFailure(call: Call<List<GetSatScoreItem>>, t: Throwable) {
                getSatScoreItem.addAll(emptyList())
                unableToFetchData()
            }

            override fun onResponse(
                call: Call<List<GetSatScoreItem>>,
                response: Response<List<GetSatScoreItem>>
            ) {
                response.body()?.let { getSatScoreItem.addAll(it) }
                progressBarVisibility(View.GONE)
                textViewVisibility(View.VISIBLE)
                //First check Whether the bdn id(which we got form the previous class) is present in response
                if (getSatScoreItem.any { it.dbn == dbn }){
                    for (position in 0 until getSatScoreItem.size){
                        if (getSatScoreItem[position].dbn == dbn){
                            satReadingScoreDetails.text = getSatScoreItem[position].satCriticalReadingAvgScore
                            satMathScoreDetails.text = getSatScoreItem[position].satMathAvgScore
                            satWritingScoreDetails.text = getSatScoreItem[position].satWritingAvgScore
                            // Add break because there will be only one dbn id is associated with each school
                            break
                        }
                    }
                }else{
                    //If there is no matching bdn id display below
                   unableToFetchData()
                }
            }
        })
    }

    private fun textViewVisibility(visible: Int) {
        satReadingScoreDetails.visibility = visible
        satMathScoreDetails.visibility = visible
        satWritingScoreDetails.visibility = visible
    }

    private fun progressBarVisibility(visible: Int) {
        progressBarWriting.visibility = visible
        progressBarReading.visibility = visible
        progressBarMath.visibility = visible

    }

    private fun unableToFetchData() {
        satReadingScoreDetails.text = "N/A"
        satMathScoreDetails.text = "N/A"
        satWritingScoreDetails.text = "N/A"
        ViewUtil.showToast(applicationContext, "Unable to fetch the Scores")

    }

    // this code is for on click back icon which is located on the AppBar
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}