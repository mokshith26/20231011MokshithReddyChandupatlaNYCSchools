package com.mokshith.nyc.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.mokshith.nyc.common.ViewUtil
import com.mokshith.nyc.R
import com.mokshith.nyc.adapter.MainActivityAdapterNycHighSchoolAdapter
import com.mokshith.nyc.common.NetworkResults
import com.mokshith.nyc.viewModel.MainActivityViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var randomString: String
    private val authViewModel by viewModels<MainActivityViewModel>()

    private var mAdapter: MainActivityAdapterNycHighSchoolAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //For initial testing of dependency injection i used below 1 line code
        ViewUtil.showToast(this, randomString )

        //Connecting activity to the adapter
        mAdapter = MainActivityAdapterNycHighSchoolAdapter(this)
        val mLayoutManager = LinearLayoutManager(applicationContext)
        recyclerViewMainActivityNYCHighSchool.layoutManager = mLayoutManager
        recyclerViewMainActivityNYCHighSchool.itemAnimator = DefaultItemAnimator()
        recyclerViewMainActivityNYCHighSchool.adapter = mAdapter

        // First check if internet is available
        if (ViewUtil.isNetworkAvailable(this)){
            //Calling the api to get get data
            intiViewModelHilt()
        }else{
            ViewUtil.showToast(this, "There is no Internet please try again")
        }



        authViewModel.highSchoolLiveData.observe(this) {
            when (it) {
                is NetworkResults.Success -> {
                    progressBarMainActivity.visibility = View.GONE
                    it.data?.let { it1 -> mAdapter?.setSchoolData(it1) }
                }

                is NetworkResults.Error -> {
                    progressBarMainActivity.visibility = View.GONE
                    ViewUtil.showToast(this, "Something went wrong try again")
                }
                else -> {
                    progressBarMainActivity.visibility = View.GONE
                    ViewUtil.showToast(this, "Something went wrong try again")
                }
            }
        }
    }

    //In the code is used with dependency injection
    private fun intiViewModelHilt() {
        progressBarMainActivity.visibility = View.VISIBLE
        authViewModel.makeApiCallHilt()
    }

    // The code is not used its just reference how we can do with out dependency injection
    private fun intiViewModel() {
        progressBarMainActivity.visibility = View.VISIBLE
        //Used MVVM architecture, Implemented View Model
        val viewModel = ViewModelProvider(this)[MainActivityViewModel::class.java]
        viewModel.getLiveData().observe(this) {
            progressBarMainActivity.visibility = View.GONE
            if (it != null) {
                mAdapter?.setSchoolData(it)
                //Log.e("TAG", "intiViewModel: ${it.size}",)
            } else {
                ViewUtil.showToast(this, "Empty List")
            }
        }
        viewModel.makeAPICall()
    }
}