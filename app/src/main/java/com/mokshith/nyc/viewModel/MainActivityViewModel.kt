package com.mokshith.nyc.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mokshith.nyc.common.NetworkResults
import com.mokshith.nyc.di.UserRepository
import com.mokshith.nyc.modelClasses.NycHighSchoolsModel
import com.mokshith.nyc.retrofitServices.RetrofitClient
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(private val userRepository: UserRepository):ViewModel() {

    val highSchoolLiveData: LiveData<NetworkResults<List<NycHighSchoolsModel>>>
        get() = userRepository.highSchoolLiveData

    fun makeApiCallHilt(){
        //I have used coroutines here to to make the network calls in light wt threads. I used viewModelScope because it avoids memory leaks
        viewModelScope.launch {
            userRepository.getNycHighSchoolsData()
        }
    }


    //All the below part is not used its just for the reference how i wrote the code with out using dependency injection
    var liveDataList: MutableLiveData<List<NycHighSchoolsModel>> = MutableLiveData()

    fun getLiveData(): MutableLiveData<List<NycHighSchoolsModel>>{
        return liveDataList
    }

    fun makeAPICall(){
        val service = RetrofitClient().webService
        val call = service.getNycHighSchoolsData()
        call.enqueue(object : Callback<List<NycHighSchoolsModel>>{
            override fun onFailure(call: Call<List<NycHighSchoolsModel>>, t: Throwable) {
                liveDataList.postValue(null)
            }
            override fun onResponse(
                call: Call<List<NycHighSchoolsModel>>,
                response: Response<List<NycHighSchoolsModel>>
            ) {
                liveDataList.postValue(response.body())
            }
        })
    }

}